import sun.misc.IOUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        List<String> list = new ArrayList<String>();
        List<String> list2 = new ArrayList<String>();

        System.out.println("Dekodowanie wybranego obrazu Heatmapy i zlaczenie z wybranym pano\n");
        System.out.println("Logi: ");
        printdir("","*.txt",list);


        System.out.println("List: ");
        for(int i = 0; i<list.size();i++) {
            System.out.println(list.get(i));
        }

        Scanner pathfinder = new Scanner(System.in);

        System.out.println("\npodaj nazwe bez rozszerzenia: ");
        String logPath= pathfinder.nextLine();
        String logPathtxt= "log/" + logPath+".txt";

        Scanner logDecode = new Scanner( new File(logPathtxt) );
        String text = logDecode.useDelimiter("\\A").next();

        //System.out.println(text);
        logDecode.close();

        String base64Image = text.split(",")[1];
        byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(base64Image);
        BufferedImage img = ImageIO.read(new ByteArrayInputStream(imageBytes));

        File file = new File(logPath + ".png");
        ImageIO.write(img,"png",file);
        System.out.println("\nPanoramy: ");
        printdir("pano","*.jpg",list2);

        System.out.println("\npodaj nazwe pano bez rozszerzenia: ");
        String panoimg = pathfinder.nextLine();
        String panoimgjpg ="pano/" +  panoimg+".jpg";

        BufferedImage img1 = ImageIO.read(new File(panoimgjpg));
        BufferedImage img2 = ImageIO.read(new File(file.toString()));

        int w = Math.max(img1.getWidth(), img2.getWidth());
        int h = Math.max(img1.getHeight(), img2.getHeight());

        File fileCombined = new File(panoimg + file);
        BufferedImage combined = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

        Graphics g = combined.getGraphics();
        g.drawImage(img1, 0, 0, null);
        g.drawImage(img2, 0, 0, null);
        ImageIO.write(combined,"png",fileCombined);
    }
    public static void printdir(String path,String ext,List<String> list){
        Path path1 = Paths.get(path);
        try {
            DirectoryStream<Path> ds = Files.newDirectoryStream(path1, ext);
            for (Path entry: ds) {
                System.out.println(entry);
                list.add(entry.toString());

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
